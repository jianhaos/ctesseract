import PackageDescription

let package = Package(
    name: "cTesseract",
    pkgConfig: "tesseract",
    providers: [
        .Brew("tesseract"),
        .Apt("tesseract-ocr")
    ]
)

